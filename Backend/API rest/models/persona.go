package models

import "time"

//Desde aqui generamos la tabla con gorm (no es necesaria generarla de manera manual en mysql pero si hay que tener la base de datos donde sera alojada)
type Persona struct {
	ID        int64     `json:"id" gorm:"primary_key;auto_increment"`
	TypeID    string    `json:"typeid"`
	Doc       string    `json:"doc"`
	FirstName string    `json:"firstname"`
	LastName  string    `json:"lastname"`
	Email     string    `json:"email"`
	Country   string    `json:"country"`
	Area      string    `json:"area"`
	State     bool      `json:"state"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

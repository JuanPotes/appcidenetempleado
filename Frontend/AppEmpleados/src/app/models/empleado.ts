export class Empleado{
    id?: number
    typeid?: string
    date?: Date
    doc?: string
    firstname?: string
    lastname?: string
    email?: string
    country?: string
    area?: string  
}
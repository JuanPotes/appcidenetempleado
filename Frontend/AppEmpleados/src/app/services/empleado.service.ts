import { Injectable } from '@angular/core';
import { Empleado } from '../models/empleado';

@Injectable({
  providedIn: 'root'
})
export class EmpleadoService {

  listEmpleado: Empleado[] = [
    {typeid: "Cedula de ciudadania",date: new Date(),doc:"1000123453", firstname: "Carlos David", lastname: "Correa Vargas", email: "carlos.correa@cidenet.com.co", country: "Colombia", area: "Administracion"},
    {typeid: "Pasaporte",date: new Date('2021-03-04'),doc:"1100333124", firstname: "Juan David", lastname: "Potes Medina", email: "juan.potes@cidenet.com.co", country: "Colombia", area: "Talento Humano"},
    {typeid: "Pasaporte",date: new Date('2021-06-12'),doc:"765F-1234a12", firstname: "Jhon", lastname: "Fisher Tempenny", email: "jhon.fisher@cidenet.com.us", country: "Estados Unidos", area: "Compras"},
    {typeid: "Cedula de ciudadania",date: new Date(),doc:"Q123-ASW-123A", firstname: "Veronica", lastname: "Cooper Salvatore", email: "veronica.cooper@cidenet.com.us", country: "Estados Unidos", area: "Financiera"},
    {typeid: "Cedula de ciudadania",date: new Date(),doc:"1000657823", firstname: "Camila", lastname: "Belen Barrios", email: "camila.belen@cidenet.com.co", country: "Colombia", area: "Financiera"},
    {typeid: "Cedula de extranjeria",date: new Date(),doc:"3444212301", firstname: "Camila", lastname: "Belen Hoyos", email: "camila.belen1@cidenet.com.co", country: "Colombia", area: "Compras"},
    {typeid: "Cedula de extranjeria",date: new Date(),doc:"AOW-12FaW", firstname: "Stephan", lastname: "Portnoy Darell", email: "stephan.portnoy@cidenet.com.us", country: "Estados Unidos", area: "Compras"},
    {typeid: "Permiso Especial",date: new Date(),doc:"100023123", firstname: "Mike", lastname: "Morrison Dimag", email: "mike.morrison@cidenet.com.us", country: "Estados Unidos", area: "Servicios Varios"},
    {typeid: "Cedula de extranjeria",date: new Date(),doc:"FF101023-1", firstname: "Megan", lastname: "Smith Jones", email: "megan.smith@cidenet.com.us", country: "Estados Unidos", area: "Servicios Varios"},
    {typeid: "Permiso Especial",date: new Date(),doc:"AOW-12FaW", firstname: "Megan", lastname: "Smith Miller", email: "stephan.smith1@cidenet.com.us", country: "Estados Unidos", area: "Servicios Varios"}
  ]

  constructor() { }

  getEmpleados(){
    return this.listEmpleado.slice()
  }

  eliminarEmpleado(index: number){
    this.listEmpleado.splice(index, 1)
  }

  agregarEmpleado(empleado: Empleado){
    this.listEmpleado.unshift(empleado)
  }

  getEmpleado(index:number){
    return this.listEmpleado[index]
  }

  editEmpleado(empleado: Empleado, idEmpleado: number){
      this.listEmpleado[idEmpleado].typeid = empleado.typeid
      this.listEmpleado[idEmpleado].date = empleado.date
      this.listEmpleado[idEmpleado].doc = empleado.doc
      this.listEmpleado[idEmpleado].firstname = empleado.firstname
      this.listEmpleado[idEmpleado].lastname = empleado.lastname
      this.listEmpleado[idEmpleado].email = empleado.email
      this.listEmpleado[idEmpleado].country = empleado.country
      this.listEmpleado[idEmpleado].area = empleado.area
  }
}

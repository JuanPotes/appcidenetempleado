import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { Empleado } from '../../models/empleado';
import { EmpleadoService } from '../../services/empleado.service';

@Component({
  selector: 'app-add-edit-empleado',
  templateUrl: './add-edit-empleado.component.html',
  styleUrls: ['./add-edit-empleado.component.css']
})
export class AddEditEmpleadoComponent implements OnInit {

  pais: any[] = ["Colombia", "Estados Unidos"]
  typeid: any[] = ["Cedula de ciudadania", "Cedula de extranjeria", "Pasaporte", "Permiso Especial"]
  area: any[] = ["Administracion", "Financiera", "Compras", "Infrastructura", "Operacion", "Talento Humano", "Servicios Varios"]
  idEmpleado: any
  accion = 'Crear'

  myForm: FormGroup

  constructor(private fb: FormBuilder,
              private EmpleadoService: EmpleadoService,
              private route: Router,
              private snackBar: MatSnackBar,
              private aRoute: ActivatedRoute) {
    this.myForm=this.fb.group({
      typeid: ['', [Validators.required]],
      date: ['', [Validators.required]],
      doc: ['',[Validators.required, Validators.maxLength(20)]],
      firstname: ['', [Validators.required, Validators.maxLength(40)]],
      lastname: ['', [Validators.required, Validators.maxLength(40)]],
      email: ['',[Validators.required, Validators.email]],
      country: ['',[Validators.required]],
      area: ['', [Validators.required]]
    })
    const idParam = 'id'
    this.idEmpleado = this.aRoute.snapshot.params[idParam]
   }

  ngOnInit(): void {
    if (this.idEmpleado !== undefined){
      this.accion = 'Editar'
      this.esEditar()
    }
  }

  guardarEmpleado(){
    const empleado: Empleado = {
      typeid: this.myForm.get('typeid')!.value,
      date: this.myForm.get('date')!.value,
      doc: this.myForm.get('doc')!.value,
      firstname: this.myForm.get('firstname')!.value,
      lastname: this.myForm.get('lastname')!.value,
      email: this.myForm.get('email')!.value,
      country: this.myForm.get('country')!.value,
      area: this.myForm.get('area')!.value
    }

    if (this.idEmpleado != undefined){
      this.editarEmpleado(empleado)
    }else{
      this.agregarEmpleado(empleado)
    }

    
  }

  agregarEmpleado(empleado: Empleado){
    this.EmpleadoService.agregarEmpleado(empleado)
    this.snackBar.open('El empleado ha sido creado con exito!','',{
      duration: 3000
    })
    this.route.navigate(['/'])
  }

  editarEmpleado(empleado: Empleado){
      this.EmpleadoService.editEmpleado(empleado,this.idEmpleado)
      this.snackBar.open('El empleado ha sido actualizado con exito!','',{
        duration: 3000
      })
      this.route.navigate(['/'])
  }

  esEditar(){
    const empleado: Empleado = this.EmpleadoService.getEmpleado(this.idEmpleado)
    this.myForm.patchValue({
      typeid: empleado.typeid,
      date: empleado.date,
      doc: empleado.doc,
      firstname: empleado.firstname,
      lastname: empleado.lastname,
      email: empleado.email,
      country: empleado.country,
      area: empleado.area,
    })
  }

}
